// file : intersect.hpp
//
// This file is part of Flux2D.
//
// Flux2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Flux2D is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with Flux2D. If not, see <www.gnu.org/licenses>.

#ifndef FLUX2D_INTERSECT_HPP
#define FLUX2D_INTERSECT_HPP

// Flux2D
#include <flux2d/body.hpp>

// GLM
#include <glm/glm.hpp>

// Standard
#include <vector>

namespace Flux2D
{
	struct Intersect
	{
		glm::vec2 normal;
		glm::vec2 position;
		float     depth;

		Intersect(glm::vec2 norm = glm::vec2(0, 0), glm::vec2 pos = glm::vec2(0, 0), float depth = 0)
		{
			this->normal   = norm;
			this->position = pos;
			this->depth = depth;
		}
	};
}

#endif
