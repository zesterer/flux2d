// file : body.hpp
//
// This file is part of Flux2D.
//
// Flux2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Flux2D is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with Flux2D. If not, see <www.gnu.org/licenses>.

#ifndef FLUX2D_BODY_HPP
#define FLUX2D_BODY_HPP

// Flux2D
#include <flux2d/intersect.hpp>
#include <flux2d/form.hpp>

// GLM
#include <glm/glm.hpp>

// Standard
#include <vector>
#include <iostream>

namespace Flux2D
{
	class Body
	{
	private:
		glm::vec2 pos  = glm::vec2(0, 0); // Position : m
		glm::vec2 lpos = glm::vec2(0, 0); // Last position : m
		glm::vec2 lvel = glm::vec2(0, 0); // Derived velocity : m/s
		glm::vec2 lacc = glm::vec2(0, 0); // Derived acceleration : m/s^2
		float     rot  = 0;               // Rotation : rad
		float     lrot = 0;               // Last rotation : rad
		float     avel = 0;               // Derived angular velocity : rad / s

		float mass     = 1.0; // Mass : Kg
		float rest     = 0.5; // Coefficient of restitution
		float statFric = 0.5;  // Coefficient of static friction
		float dynFric  = 2;   // Coefficient of dynamic friction

		glm::vec2 cTranslate = glm::vec2(0, 0); // Net translation (for this integration) : m
		glm::vec2 cForce     = glm::vec2(0, 0); // Net force (for this integration step) : M*m/s^2
		glm::vec2 cImpulse   = glm::vec2(0, 0); // Net impulse (for this integration step) : M*m/s

		float cAForce   = 0.0f; // Net angular force : M*rad/s^2
		float cAImpulse = 0.0f; // Net angular force : M*rad/s

		FormType formType = FormType::CIRCLE;
		union
		{
			FormCircle formCircle;
		};
		AABB bounds;

	public:
		Body(FormType formType = FormType::CIRCLE, glm::vec2 pos = glm::vec2(0, 0))
		{
			this->pos = pos;
			this->lpos = this->pos;

			this->formType = formType;
			switch (this->formType)
			{
			case FormType::CIRCLE:
				this->formCircle = FormCircle(0);
				break;
			default:
				break;
			}

			this->rot = 0.1;
			this->deriveAttributes(0.1);
		}

		glm::vec2 getPosition() const
		{
			return this->pos;
		}

		float getRotation() const
		{
			return this->rot;
		}

		glm::vec2 getVelocity() const
		{
			return this->lvel;
		}

		float getAngularVelocity() const
		{
			return this->avel;
		}

		float getMass() const
		{
			return this->mass;
		}

		glm::vec2 getMomentum() const
		{
			return this->getVelocity() * this->mass;
		}

		glm::vec2 getMomentumAt(glm::vec2 pos) const
		{
			glm::vec2 rT = glm::vec2(this->pos.y - pos.y, pos.x - this->pos.x);
			return (this->getVelocity() + this->getAngularVelocity() * rT) * this->mass;
		}

		AABB getBounds() const
		{
			return this->bounds;
		}

		FormCircle& getFormCircle()
		{
			return this->formCircle;
		}

		void setPosition(glm::vec2 pos)
		{
			this->pos = pos;
		}

		void setMass(float mass)
		{
			this->mass = mass;
		}

		void setDensity(float density)
		{
			switch (this->formType)
			{
			case FormType::CIRCLE:
				this->mass = this->formCircle.getArea() * density;
				break;
			default:
				break;
			}
		}

		void translate(glm::vec2 translation)
		{
			this->cTranslate += translation;
		}

		void applyForce(glm::vec2 force)
		{
			this->cForce += force;
		}

		void applyImpulse(glm::vec2 impulse)
		{
			this->cImpulse += impulse;
		}

		void applyTorque(float force)
		{
			this->cAForce += force;
		}

		void applyAngularImpulse(float impulse)
		{
			this->cAImpulse += impulse;
		}

		void deriveAttributes(float delta)
		{
			this->lvel = (this->pos - this->lpos) / delta; // Derive linear velocity
			this->avel = fmod(this->rot - this->lrot + M_PI * 2 * 10000, M_PI * 2) / delta; // Derive angular velocity

			// Find bounds
			switch (this->formType)
			{
			case FormType::CIRCLE:
				{
					this->bounds = this->formCircle.getBounds();
					this->bounds.offset += this->pos;

				}
			default:
				this->bounds = AABB();
			}
		}

		void integrate(float delta)
		{
			glm::vec2 acc  = glm::vec2(0, 0);
			float     aacc = 0;

			// Apply force
			acc += (this->cForce) / this->mass;

			// Apply impulse
			acc += this->cImpulse / (this->mass * delta);

			this->lacc = acc;

			// Update position
			glm::vec2 npos = this->pos + (this->getVelocity() + acc * delta) * delta;
			this->lpos = this->pos + this->cTranslate;
			this->pos = npos + this->cTranslate;

			// Update rotation
			float nrot = this->rot + (this->getAngularVelocity() + aacc) * delta;
			this->lrot = this->rot;
			this->rot  = nrot;

			this->deriveAttributes(delta);

			// Reset instantaneous totals
			this->cForce = glm::vec2(0, 0);
			this->cImpulse = glm::vec2(0, 0);
			this->cTranslate = glm::vec2(0, 0);
		}

		Intersect getIntersect(const Body& other) const
		{
			if (this->formType == FormType::CIRCLE && other.formType == FormType::CIRCLE)
			{
				float sumRadius = this->formCircle.radius + other.formCircle.radius;
				glm::vec2 norm = other.getPosition() - this->pos;
				float     pen = sumRadius - glm::distance(this->pos, other.getPosition());
				return Intersect(glm::normalize(norm), glm::vec2(0, 0), pen);
			}

			return Intersect();
		}

		void handleCollisionResponse(Body& other, Intersect intersect, float delta)
		{
			if (intersect.depth > 0)
			{
				glm::vec2 rVel = other.getVelocity() - this->getVelocity();
				float nVel = glm::dot(rVel, intersect.normal);

				if (nVel > 0) // End if objects are moving away
					return;

				float mRest = glm::min(this->rest, other.rest);
				float impScalar = ((1 + mRest) * nVel) / (1 / this->mass + 1 / other.mass);

				// Reflection
				{
					glm::vec2 reflectImpulse = impScalar * intersect.normal;

					this->applyImpulse(reflectImpulse);
					other.applyImpulse(-reflectImpulse);
				}
				// Friction
				/*{
					glm::vec2 rForce = this->getVelocity() * this->mass - other.getVelocity() * other.mass;
					float mu = 0.9f;//glm::length(glm::vec2(this->statFric, other.statFric));
					glm::vec2 tangent = glm::vec2(intersect.normal.y, -intersect.normal.x);

					glm::vec2 fricImpulse = tangent * mu * glm::dot(rForce, tangent);

					//glm::vec2 tangent = glm::normalize(rVel - glm::dot(lForce, intersect.normal) * intersect.normal);
					//float fricScalar = -(glm::dot(rVel, tangent)) / (1 / this->mass + 1 / other.mass);
					//if (glm::abs(fricScalar) <= mu * glm::dot(lForce, intersect.normal))
					//	fricImpulse = glm::dot(lForce, tangent);
					//else
					//{
					//	fricImpulse = glm::dot(lForce, tangent) * glm::length(glm::vec2(this->dynFric, other.dynFric));
					//}

					this->applyForce(fricImpulse);
					other.applyForce(-fricImpulse);
				}*/

				/*float fricCo = 10.9;
				std::cout << glm::length(this->lacc) << std::endl;
				float fricScalar = fricCo * glm::dot(this->lacc, intersect.normal);
				glm::vec2 coNormal = glm::vec2(-intersect.normal.y, intersect.normal.x);
				glm::vec2 friction = fricScalar * glm::normalize(glm::dot(this->lacc, -coNormal) * coNormal);
				this->applyForce(friction);
				other.applyForce(-friction);*/
			}
		}

		void handleCollision(Body& other, float delta)
		{
			if (this->formType == FormType::CIRCLE && other.formType == FormType::CIRCLE)
			{
				Intersect intersect = this->getIntersect(other);
				float netMass = this->mass + other.mass;

				if (intersect.depth > 0)
				{
					float damping = 0.9f;
					this->translate(-(this->mass / netMass) * intersect.normal * intersect.depth * damping);
					other.translate(+(other.mass / netMass) * intersect.normal * intersect.depth * damping);

					this->handleCollisionResponse(other, intersect, delta);
				}
			}
		}

		void handleWallCollision(glm::vec2 size, float delta)
		{
			if (this->formType == FormType::CIRCLE)
			{
				if (this->pos.x < this->formCircle.radius)
				{
					this->pos.x = this->formCircle.radius;
					this->applyImpulse(glm::vec2(glm::abs(this->getVelocity().x), 0) * (1.0f + this->rest) * this->mass);
				}
				if (this->pos.y < this->formCircle.radius)
				{
					this->pos.y = this->formCircle.radius;
					this->applyImpulse(glm::vec2(0, glm::abs(this->getVelocity().y)) * (1.0f + this->rest) * this->mass);
				}
				if (this->pos.x > size.x - this->formCircle.radius)
				{
					this->pos.x = size.x - this->formCircle.radius;
					this->applyImpulse(glm::vec2(-glm::abs(this->getVelocity().x), 0) * (1.0f + this->rest) * this->mass);
				}
				if (this->pos.y > size.y - this->formCircle.radius)
				{
					this->pos.y = size.y - this->formCircle.radius;
					this->applyImpulse(glm::vec2(0, -glm::abs(this->getVelocity().y)) * (1.0f + this->rest) * this->mass);
				}
			}
		}
	};
}

#endif
