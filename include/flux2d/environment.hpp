// file : environment.hpp
//
// This file is part of Flux2D.
//
// Flux2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Flux2D is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with Flux2D. If not, see <www.gnu.org/licenses>.

#ifndef FLUX2D_ENVIRONMENT_HPP
#define FLUX2D_ENVIRONMENT_HPP

// Flux2D
#include <flux2d/body.hpp>

// Standard
#include <vector>
#include <iostream>

namespace Flux2D
{
	class Environment
	{
	private:
		std::vector<Body> bodies;
		glm::vec2 gravity = glm::vec2(0, 0);

	public:
		template <typename ... Args>
		Body& addBody(Args ... args)
		{
			this->bodies.emplace_back(args ...);
			return this->bodies.back();
		}

		void setGravity(glm::vec2 gravity)
		{
			this->gravity = gravity;
		}

		void simulate(float delta)
		{
			for (Body& body : this->bodies)
			{
				body.applyForce(this->gravity * body.getMass()); // Gravity
				body.integrate(delta);
			}

			for (size_t i = 0; i < this->bodies.size(); i ++)
			{
				for (size_t j = i + 1; j < this->bodies.size(); j ++)
				{
					AABB aabb0 = this->bodies[i].getBounds();
					AABB aabb1 = this->bodies[j].getBounds();

					if (aabb0.isIntersecting(aabb1))
						this->bodies[i].handleCollision(this->bodies[j], delta);
				}
			}

			for (Body& body : this->bodies)
				body.handleWallCollision(glm::vec2(100, 60), delta);
		}

		size_t bodyCount()
		{
			return this->bodies.size();
		}

		Body& getBody(size_t i)
		{
			return this->bodies[i];
		}
	};
}

#endif
