// file : form.hpp
//
// This file is part of Flux2D.
//
// Flux2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Flux2D is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with Flux2D. If not, see <www.gnu.org/licenses>.

#ifndef FLUX2D_FORM_HPP
#define FLUX2D_FORM_HPP

// Flux2D
#include <flux2d/aabb.hpp>

// GLM
#include <glm/glm.hpp>

// Standard
#include <vector>

namespace Flux2D
{
	enum class FormType
	{
		CIRCLE,
	};

	struct FormCircle
	{
		float radius;

		FormCircle(float radius = 0.0f)
		{
			this->radius = radius;
		}

		float getArea() const
		{
			return M_PI * this->radius * this->radius;
		}

		AABB getBounds() const
		{
			return AABB(glm::vec2(-this->radius), glm::vec2(this->radius * 2));
		}
	};
}

#endif
