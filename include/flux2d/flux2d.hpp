// file : flux2d.hpp
//
// This file is part of Flux2D.
//
// Flux2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Flux2D is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with Flux2D. If not, see <www.gnu.org/licenses>.

#ifndef FLUX2D_FLUX2D_HPP
#define FLUX2D_FLUX2D_HPP

// Flux2D
#include <flux2d/environment.hpp>

namespace Flux2D
{
	void say_hello();
}

#endif
