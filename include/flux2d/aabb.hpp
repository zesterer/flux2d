// file : form.hpp
//
// This file is part of Flux2D.
//
// Flux2D is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as
// published by the Free Software Foundation, either version 3 of
// the License, or (at your option) any later version.
//
// Flux2D is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public
// License along with Flux2D. If not, see <www.gnu.org/licenses>.

#ifndef FLUX2D_AABB_HPP
#define FLUX2D_AABB_HPP

// GLM
#include <glm/glm.hpp>

// Standard
#include <vector>

namespace Flux2D
{
	struct AABB
	{
		glm::vec2 offset;
		glm::vec2 size;

		AABB(glm::vec2 offset = glm::vec2(0), glm::vec2 size = glm::vec2(0))
		{
			this->offset = offset;
			this->size = size;
		}

		float isIntersecting(const AABB& other) const
		{
			return ((this->offset.x + this->size.x >= other.offset.x) && (other.offset.x + other.size.x >= this->offset.x)) &&
			       ((this->offset.y + this->size.y >= other.offset.y) && (other.offset.y + other.size.y >= this->offset.y));
		}


	};
}

#endif
