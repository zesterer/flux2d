// Flux2D
#include <flux2d/flux2d.hpp>

// SFML
#include <SFML/Graphics.hpp>

// Standard
#include <iostream>
#include <cstdlib>

int main()
{
	Flux2D::Environment env;

	srand(2);

	for (int i = 0; i < 30; i ++)
	{
		Flux2D::Body& body = env.addBody();
		body.setPosition(glm::vec2(rand() % 100, rand() % 60));
		body.getFormCircle().radius = (32 + rand() % 128) / 50.0f;
		body.setDensity(1);
		body.applyImpulse(glm::vec2(rand() % 24 - 12, rand() % 24 - 12));
	}

	sf::RenderWindow win(sf::VideoMode(1000, 600), "Flux2D Demo");
	win.setFramerateLimit(60);

	sf::Texture woodTexture;
	woodTexture.loadFromFile("wood.jpg");

		while (win.isOpen())
		{
			sf::Event event;
			while (win.pollEvent(event))
			{
				if (event.type == sf::Event::Closed)
					win.close();
				else if (event.type == sf::Event::KeyPressed)
				{
					switch (event.key.code)
					{
					case sf::Keyboard::Left:
						env.setGravity(glm::vec2(-9.81, 0));
						break;
					case sf::Keyboard::Right:
						env.setGravity(glm::vec2(9.81, 0));
						break;
					case sf::Keyboard::Up:
						env.setGravity(glm::vec2(0, -9.81));
						break;
					case sf::Keyboard::Down:
						env.setGravity(glm::vec2(0, 9.81));
						break;
					default:
						break;
					}
				}
			}

			// Logic
			{
				for (int i = 0; i < 20; i ++)
					env.simulate(0.1 / 60.0);
			}

			// Graphics
			{
				win.clear(sf::Color::White);

				sf::CircleShape shape(1);

				//shape.setTexture(&woodTexture);
				shape.setOutlineThickness(2);
				shape.setOutlineColor(sf::Color::Black);

				for (size_t i = 0; i < env.bodyCount(); i ++)
				{
					Flux2D::Body& body = env.getBody(i);

					shape.setRadius(body.getFormCircle().radius * 10.0f - 2);
					shape.setFillColor(sf::Color::White);
					shape.setOrigin(shape.getRadius(), shape.getRadius());
					shape.setRotation(-body.getRotation() * 180 / M_PI);
					shape.setPosition(body.getPosition().x * 10.0f, body.getPosition().y * 10.0f);

					win.draw(shape);
				}
			}

			win.display();
		}

	return 0;
}
