// Flux2D
#include <flux2d/flux2d.hpp>

// Standard
#include <iostream>

namespace Flux2D
{
	void say_hello()
	{
		std::cout << "Hello, World!" << std::endl;
	}
}
